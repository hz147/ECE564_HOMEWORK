import UIKit

//MARK: - Tool for Input string at DukePerson creation

func seperateCommaDelimitedStringIntoStringArray(string: String) -> [String] {
    var stringArray = string.components(separatedBy: ",")
    for i in 0..<stringArray.count {
        stringArray[i] = stringArray[i].trimmingCharacters(in: .whitespaces)
    }
    return stringArray.filter { $0 != "" }
    
}
