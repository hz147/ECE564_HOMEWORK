import UIKit

class PersonInfoCellEntry {
    var image: UIImage
    var fullName: String
    var description: String
    
    init(duker: DukePerson) {
        self.image = #imageLiteral(resourceName: "avatar-male")
        if duker.gender == .Female {
            self.image = #imageLiteral(resourceName: "avatar-female")
        }
        self.fullName = "\(duker.firstName) \(duker.lastName)"
        self.description = duker.description
    }
    
    
}
