import UIKit

//MARK: - Person class declaration

public class Person: Codable {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
    
    init() {}
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(whereFrom, forKey: .whereFrom)
        try container.encode(gender, forKey: .gender)
    }
}

enum Gender : String, Codable {
    case Male = "Male"
    case Female = "Female"
}

//MARK: - Person extension: init & CodingKeys

extension Person {
    convenience init(firstName: String, lastName: String, whereFrom: String, gender: Gender, createAt: Date, imageName: String) {
        self.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
    }
    
    private enum CodingKeys: String, CodingKey {
        case firstName = "first"
        case lastName = "last"
        case whereFrom = "where"
        case gender
    }
}

//MARK: - DukePerson class declaration

public class DukePerson: Person, BlueDevil {
    
    //    MARK: - Required Components
    
    var role : DukeRole = .Student
    var hobbies : [String] = []
    var degree : AcademicDegree = .BS
    var proficientProgrammingLanguages: [String] = []
    
    // MARK: - Team Name
    
    var team = ""
    
    //    MARK: - Additional Components
    
    var numberOfHobbies: Int { return hobbies.count }
    var numberOfProficientLanguages : Int { return proficientProgrammingLanguages.count }
    var isUniversityStaff : Bool { return role != .Student }
    var firstLetterOfFirstName : String { return String(firstName.prefix(1)) }
    var luckyNumberInDouble : Double { return calculateLuckyNumber() }
    
    //    MARK: - init
    
    override init() {
        super.init()
    }
    
    init(firstName : String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, hobbies: [String], academicDegree: AcademicDegree, proficientProgrammingLanguage: [String], team: String) {
        super.init()
        initAllProperties(firstName: firstName, lastName: lastName, whereFrom: whereFrom, gender: gender, role: role, hobbies: hobbies, academicDegree: academicDegree, proficientProgrammingLanguage: proficientProgrammingLanguage, team: team)
    }
    
    func initAllProperties(firstName : String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, hobbies: [String], academicDegree: AcademicDegree, proficientProgrammingLanguage: [String], team: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
        self.role = role
        self.hobbies = hobbies
        self.degree = academicDegree
        self.proficientProgrammingLanguages = (proficientProgrammingLanguage.count > 3) ? Array(proficientProgrammingLanguage.prefix(3)) : proficientProgrammingLanguage
        self.team = team
    }
    
//    MARK: - encode & decode JSON
    
    public override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(role, forKey: .role)
        try container.encode(hobbies, forKey: .hobbies)
        try container.encode(degree, forKey: .degree)
        try container.encode(proficientProgrammingLanguages, forKey: .proficientProgrammingLanguage)
        try container.encode(team, forKey: .team)
    }
    
    required public convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            let firstName = try container.decode(String.self, forKey: .firstName)
            let lastName = try container.decode(String.self, forKey: .lastName)
            let whereFrom = try container.decode(String.self, forKey: .whereFrom)
            let gender = try container.decode(Gender.self, forKey: .gender)
            let role = try container.decode(DukeRole.self, forKey: .role)
            let hobbies = try container.decode([String].self, forKey: .hobbies)
            let academicDegree = try container.decode(AcademicDegree.self, forKey: .degree)
            let proficientProgrammingLanguage = try container.decode([String].self, forKey: .proficientProgrammingLanguage)
            let team = try container.decode(String.self, forKey: .team)
            
            initAllProperties(firstName: firstName, lastName: lastName, whereFrom: whereFrom, gender: gender, role: role, hobbies: hobbies, academicDegree: academicDegree, proficientProgrammingLanguage: proficientProgrammingLanguage, team: team)
            
        } catch {
            print(error)
        }
    }
    
    //    MARK: - print additional info(statistics)
    func calculateLuckyNumber() -> Double {
        var i = 0
        var sum : Double = 0
        while i < firstName.count {
            var str = String(firstName[firstName.index(firstName.startIndex, offsetBy: i)])
            var number:Int = 0
            for code in str.unicodeScalars {
                number = Int(code.value);
            }
            sum += Double(number)
            i+=1
        }
        return sum/Double(firstName.count)
    }
    
    func statistic() -> String {
        let fullName = "\(self.firstName) \(self.lastName)"
        let isA = (self.role != .Student) ? "is a" : "is not a"
        let pronoun = (self.gender == .Male) ? "He" : "She"
        let possessive = (self.gender == .Male) ? "His" : "Her"
        let languages = (numberOfProficientLanguages > 1) ? "languages" : "language"
        let hobbies = (numberOfHobbies > 1) ? "hobbies" : "hobby"
        return "\(fullName) \(isA) a Duke University Staff. \(possessive) first name starts with letter \(firstLetterOfFirstName). \(pronoun) has \(numberOfHobbies) \(hobbies) and \(numberOfProficientLanguages) proficient \(languages). \(possessive) lucky number is \(luckyNumberInDouble)."
    }
    
    func whoIs() -> String {
        return description
    }
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}

enum DukeRole : String, Codable {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

enum AcademicDegree : String, Codable {
    case BS = "BS"
    case MS = "MS"
    case MENG = "MENG"
    case NA = "Not Applicable"
    case Other = "Other"
}

//MARK: - DukerPerson extension: CodingKeys

extension DukePerson {
    private enum CodingKeys: String, CodingKey {
        case firstName = "first"
        case lastName = "last"
        case whereFrom = "where"
        case gender
        case role
        case hobbies
        case degree = "degree"
        case proficientProgrammingLanguage = "proficientProgrammingLanguages"
        case team
    }
}


//MARK: - protocal: CustomStringConvertible

extension DukePerson: CustomStringConvertible {
    public var description: String { return self.buildInfo() }
    
    func buildInfo() -> String {
        let pronoun = self.gender == .Male ? "He" : "She"
        let hobbiesString = (self.hobbies.count != 0) ? self.stringListArray2String(stringListArray: self.hobbies) : "nothing"
        let proficientLanguagesString = (self.proficientProgrammingLanguages.count != 0) ?self.stringListArray2String(stringListArray:self.proficientProgrammingLanguages) : "no programming languages"
        
        return "\(fullName) is from \(whereFrom) and is a \(role.rawValue). \(pronoun) is proficient in \(proficientLanguagesString). When not in class, \(firstName) enjoys \(hobbiesString)."
    }
    
    var fullName: String { return "\(self.firstName) \(self.lastName)" }
    
    func stringListArray2String(stringListArray: [String]) -> String {
        let comma = ","
        let space = " "
        let connector = " and "
        
        var string = ""
        var tailIndex = stringListArray.count
        var index = 0
        while tailIndex > 0 {
            let element = stringListArray[index]
            switch(tailIndex) {
            case 1: string += element
            case 2: string += element + connector
            default: string += element + comma + space
            }
            tailIndex -= 1
            index += 1
        }
        
        return string
    }
}

//MARK: - whoIs function

func whoIs(_ fullNameToSearch : String, Dukers: [DukePerson]) -> String {
    for i in 0..<Dukers.count {
        let fullName = "\(Dukers[i].firstName) \(Dukers[i].lastName)"
        if fullName == fullNameToSearch {
            return Dukers[i].whoIs()
        }
    }
    return "I haven't found a person called \(fullNameToSearch)"
}

//MARK: - create DukePerson; find/sort DukePerson array

func createDukePersonFromStringsWithErrorCheck(firstName : String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, hobbiesInString: String, degree: AcademicDegree, proficientProgrammingLanguagesInString: String, team: String) -> DukePerson? {
    if firstName.isEmpty || lastName.isEmpty || whereFrom.isEmpty{
        return nil
    }
    
    let hobbies = seperateCommaDelimitedStringIntoStringArray(string: hobbiesInString)
    let languages = seperateCommaDelimitedStringIntoStringArray(string: proficientProgrammingLanguagesInString)
    
    let teamName = (role == .Student) ? team : ""
    
    return DukePerson(firstName: firstName, lastName: lastName, whereFrom: whereFrom, gender: gender, role: role, hobbies: hobbies, academicDegree: degree, proficientProgrammingLanguage: languages, team: teamName)
}

//MARK: - Team class

class Team: Codable {
    var name = ""
    var members = [DukePerson]()
    
    //MARK: - initializers
    init(members: [DukePerson]) {
        if members.count > 0 {
            if let firstMember = members.first {
                if firstMember.role == .Professor || firstMember.role == .TA || (firstMember.role == .Student && firstMember.team == "") {
                    self.name = firstMember.role.rawValue
                    self.members = members
                } else {
                    self.name = firstMember.team
                    self.members = members
                }
            }
        }
    }
    
    init(name: String) {
        self.name = name
    }
    
    //MARK: - functions: add
    
    func addMember(member: DukePerson) {
        members.append(member)
    }
    
    func eligibleToAdd(person: DukePerson) -> Bool {
        var hisTeam: String
        if person.role == .Professor || person.role == .TA || (person.role == .Student && person.team == "") {
            hisTeam = person.role.rawValue
        } else {
            hisTeam = person.team
        }
        return (hisTeam == name)
    }
    
//    MARK: functions: sort
    
    func sort(sortOrder: SortOrder) {
        if sortOrder == .byASC {
            members.sort(by: { $0.firstName < $1.firstName })
        } else {
            members.sort(by: { $0.firstName > $1.firstName })
        }
    }
    
    //MARK: - fucntions: status
    
    func doesExist(person: DukePerson) -> Bool {
        for member in members {
            if person.fullName == member.fullName {
                return true
            }
        }
        return false
    }
    
    func isEmpty() -> Bool {
        return (members.count == 0)
    }
    
    var count: Int {
        return members.count
    }
    
    //MARK: - return the row of the person's full-name
    
    func returnRow(name: String) -> Int? {
        for (i, person) in self.members.enumerated() {
            if person.fullName == name {
                return i
            }
        }
        return nil
    }
}

enum SortOrder {
    case byASC
    case byDESC
}


//MARK: - Team class Codable

extension Array where Element == Team {
    func saveAsJson() {
        let savedDataDirectoryURL = URL(fileURLWithPath: "savedData", relativeTo: FileManager.documentDirectoryURL)
        let savedDataURL = savedDataDirectoryURL
            .appendingPathComponent("savedData")
            .appendingPathExtension("json")
        
        try? FileManager.default.createDirectory(at: savedDataDirectoryURL, withIntermediateDirectories: true)
        
        do {
            let jsonEncoder = JSONEncoder()
            jsonEncoder.outputFormatting = .prettyPrinted
            let jsonData = try jsonEncoder.encode(self)
            
            try jsonData.write(
                to: savedDataURL,
                options: .atomic
            )
        } catch {
            print(error)
        }
    }
    
    static func loadFromJson() -> [Team]? {
        let savedDataDirectoryURL = URL(fileURLWithPath: "savedData", relativeTo: FileManager.documentDirectoryURL)
        let savedDataURL = savedDataDirectoryURL
            .appendingPathComponent("savedData")
            .appendingPathExtension("json")
        
        do {
            let jsonDecoder = JSONDecoder()
            let savedJsonData = try Data(contentsOf: savedDataURL)
            let jsonTeams = try jsonDecoder.decode([Team].self, from: savedJsonData)
            
            return jsonTeams
        } catch {
            print(error)
        }
        return nil
    }
    
    static func checkIfSavedJsonExists() -> Bool {
        let savedDataDirectoryURL = URL(fileURLWithPath: "savedData", relativeTo: FileManager.documentDirectoryURL)
        let savedDataURL = savedDataDirectoryURL
            .appendingPathComponent("savedData")
            .appendingPathExtension("json")
        
        return FileManager.default.fileExists(atPath: savedDataURL.path)
    }
}
