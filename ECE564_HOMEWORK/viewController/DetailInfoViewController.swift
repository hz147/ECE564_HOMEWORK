import UIKit

class DetailInfoViewController: UIViewController {
    var person: DukePerson!
    var doPersonInfoDisplayAtInit: Bool! = false
    var makeAllTextFieldsIneditable: Bool! = false
    var rightBarButtonCurrentStatus: rightBarButtonStatus = .edit
    var fullName: String {
        if firstNameTextField.text == nil || lastNameTextField.text == nil {
            return ""
        }
        return "\(firstNameTextField.text!) \(lastNameTextField.text!)"
    }
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var hobbiesTextField: UITextField!
    @IBOutlet weak var languageTextField: UITextField!
    @IBOutlet weak var teamTextField: UITextField!
    @IBOutlet weak var degreeSegController: UISegmentedControl!
    @IBOutlet weak var roleSegController: UISegmentedControl!
    @IBOutlet weak var genderSegController: UISegmentedControl!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var decorationImageOnTheCorner: UIImageView!
    @IBOutlet weak var jumpToAnimationViewButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDecorationImageOnTheCorner()
        if (doPersonInfoDisplayAtInit) {
            setValueInTextFieldAndImageField()
        } else {
            enableAllInputsEdit()
        }
        if (makeAllTextFieldsIneditable) {
            disableAllInputsEdit()
        }
        // display the correct rightBarButton title
        navigationItem.rightBarButtonItem?.title = rightBarButtonCurrentStatus.rawValue
    }
}

//MARK: - perform segue

extension DetailInfoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "returnFromPersonDetailInfoByPressingSaveButton" {
            if let destVC = segue.destination as? TableViewController {
                let person = sender as! DukePerson
                if rightBarButtonCurrentStatus == .save {
                    let indexPath = destVC.selectedCellIndexPath
                    let section = indexPath!.section
                    let row = indexPath!.row
                    
                    destVC.teams[section].members.remove(at: row)
                    destVC.teams.addToClass(person: person)
                    destVC.teams.cleanEmptyTeams()
                } else if rightBarButtonCurrentStatus == .add {
                    destVC.teams.addToClass(person: person)
                }
            }
        }
    }
}

//MARK: - button action

extension DetailInfoViewController {
    
    @IBAction func whenEditOrSaveOrAddButtonPressed() {
        if rightBarButtonCurrentStatus == .edit {
            enableAllInputsEdit()
            updateRightBarButton()
        } else if rightBarButtonCurrentStatus == .save || rightBarButtonCurrentStatus == .add {
           
            if let duker = createPersonOnInput() {
                performSegue(withIdentifier: "returnFromPersonDetailInfoByPressingSaveButton", sender: duker)
            } else {
                createAlert(title: "Opps! Some inputs are incorrect", message: "Fail to add new person or modify existing person. FirstName, LastName, From cannot be empty. Gender, Role, Degree cannot be empty and must follow rules in corresponding textField.")
            }
        }
        
    }
    
    func createPersonOnInput() -> DukePerson? {
        let firstName = firstNameTextField.text!
        let lastName = lastNameTextField.text!
        let whereFrom = fromTextField.text!
        let gender: Gender = (genderSegControllerEnum(rawValue: genderSegController.selectedSegmentIndex)?.translateGenderSegControllerIndex())!
        let role: DukeRole = (roleSegControllerEnum(rawValue: roleSegController.selectedSegmentIndex)?.translateRoleSegControllerIndex())!
        let degree: AcademicDegree = (degreeSegControllerEnum(rawValue: degreeSegController.selectedSegmentIndex)?.translateDegreeSegControllerIndex())!
        let hobbies = hobbiesTextField.text!
        let languages = languageTextField.text!
        let team = teamTextField.text!
        
        return createDukePersonFromStringsWithErrorCheck(firstName: firstName, lastName: lastName, whereFrom: whereFrom, gender: gender, role: role, hobbiesInString: hobbies, degree: degree, proficientProgrammingLanguagesInString: languages, team: team)
    }
    
    @IBAction func whenCancelButtonPressed() {
        performSegue(withIdentifier: "returnFromPersonDetailInfoByPressingCancelButton", sender: nil)
    }
    
    func updateRightBarButton() {
        if rightBarButtonCurrentStatus == .edit {
            rightBarButtonCurrentStatus = .save
        }
        navigationItem.rightBarButtonItem?.title = rightBarButtonCurrentStatus.rawValue
    }
}

enum rightBarButtonStatus: String {
    case edit = "Edit"
    case save = "Save"
    case add = "Add"
}

//MARK: - create alert

extension DetailInfoViewController {
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - set init value for showing; enable & disable input

extension DetailInfoViewController {
    
    var allTextFields: [UITextField] {
        return [firstNameTextField, lastNameTextField, fromTextField, hobbiesTextField, languageTextField, teamTextField]
    }
    var allSegControllers: [UISegmentedControl] {
        return [genderSegController, roleSegController, degreeSegController]
    }
    
    func setValueInTextFieldAndImageField() {
        firstNameTextField.text! = person.firstName
        lastNameTextField.text! = person.lastName
        fromTextField.text! = person.whereFrom
        hobbiesTextField.text! = person.hobbies.joined(separator: ",")
        languageTextField.text! = person.proficientProgrammingLanguages.joined(separator: ",")
        teamTextField.text! = person.team
        switch person.gender {
        case .Male: genderSegController.selectedSegmentIndex = genderSegControllerEnum.Male.rawValue
        case .Female: genderSegController.selectedSegmentIndex = genderSegControllerEnum.Female.rawValue
        }
        switch person.role {
        case .Student: roleSegController.selectedSegmentIndex = roleSegControllerEnum.Student.rawValue
        case .TA: roleSegController.selectedSegmentIndex = roleSegControllerEnum.TA.rawValue
        case .Professor: roleSegController.selectedSegmentIndex = roleSegControllerEnum.Professor.rawValue
        }
        switch person.degree {
        case .BS: degreeSegController.selectedSegmentIndex = degreeSegControllerEnum.BS.rawValue
        case .MS: degreeSegController.selectedSegmentIndex = degreeSegControllerEnum.MS.rawValue
        case .MENG: degreeSegController.selectedSegmentIndex = degreeSegControllerEnum.MENG.rawValue
        case .NA: degreeSegController.selectedSegmentIndex = degreeSegControllerEnum.NA.rawValue
        case .Other: degreeSegController.selectedSegmentIndex = degreeSegControllerEnum.Other.rawValue
        }
        if doPersonInfoDisplayAtInit {
            imageField.isHidden = false
            if person.gender == .Male {
                imageField.image = #imageLiteral(resourceName: "avatar-male")
            } else if person.gender == .Female {
                imageField.image = #imageLiteral(resourceName: "avatar-female")
            }
        } else {
            imageField.isHidden = true
        }
    }
    
    func disableAllInputsEdit() {
        for textField in allTextFields {
            textField.isUserInteractionEnabled = false
        }
        for segController in allSegControllers {
            segController.isUserInteractionEnabled = false
        }
    }
    
    func setTextFieldsBackgroundColorToWhite() {
        for textField in allTextFields {
            textField.backgroundColor? = UIColor.white
        }
    }
    
    func enableAllInputsEdit() {
        setTextFieldsBackgroundColorToWhite()
        setTextFieldsClearButton()
        for textField in allTextFields {
            textField.isUserInteractionEnabled = true
        }
        for segController in allSegControllers {
            segController.isUserInteractionEnabled = true
        }
        if person != nil {
            teamTextField.isUserInteractionEnabled = (person.role == .Student) ? true : false
        } else {
            teamTextField.isUserInteractionEnabled = true
        }
    }
}

//MARK: - styling

extension DetailInfoViewController {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setJumpToAnimationButton(fullName: fullName)
    }
    
    func setDecorationImageOnTheCorner() {
        decorationImageOnTheCorner.image = #imageLiteral(resourceName: "dukeIcon.png")
    }
    
    func setJumpToAnimationButton(fullName: String) {
        jumpToAnimationViewButton.layer.backgroundColor = UIColor.blue.cgColor
        jumpToAnimationViewButton.titleLabel?.textColor = UIColor.white
        jumpToAnimationViewButton.layer.borderColor = UIColor.black.cgColor
        jumpToAnimationViewButton.layer.shadowOffset = CGSize.zero
        jumpToAnimationViewButton.layer.shadowRadius = 1
        jumpToAnimationViewButton.layer.shadowOpacity = 1
        jumpToAnimationViewButton.layer.shadowColor = UIColor.darkGray.cgColor
        
        if (fullName == "Haohong Zhao") && (rightBarButtonCurrentStatus != .save) {
            jumpToAnimationViewButton.isHidden = false
        } else {
            jumpToAnimationViewButton.isHidden = true
        }
    }
    
    func setTextFieldsClearButton() {
        for textField in allTextFields {
            textField.clearButtonMode = .always
        }
    }
}

//MARK: - segControllers

extension DetailInfoViewController {
    
    @IBAction func changeRoleSegController(_ sender: UISegmentedControl) {
        if roleSegControllerEnum(rawValue: sender.selectedSegmentIndex) != .Student {
            teamTextField.isUserInteractionEnabled = false
            teamTextField.text = nil
        } else {
            teamTextField.isUserInteractionEnabled = true
        }
        teamTextField.backgroundColor = (teamTextField.isUserInteractionEnabled) ? UIColor.white : UIColor.lightGray
    }
    
    enum genderSegControllerEnum: Int {
        case Male
        case Female
        
        func translateGenderSegControllerIndex() -> Gender {
            return (self == .Male) ? .Male : .Female
        }
    }
    
    private enum roleSegControllerEnum: Int {
        case Student
        case TA
        case Professor
        
        func translateRoleSegControllerIndex() -> DukeRole {
            let role: DukeRole
            switch self {
            case .Student: role = .Student
            case .TA: role = .TA
            case .Professor: role = .Professor
            }
            return role
        }
    }
    
    private enum degreeSegControllerEnum: Int {
        case BS
        case MS
        case MENG
        case NA
        case Other
        
        func translateDegreeSegControllerIndex() -> AcademicDegree {
            let degree: AcademicDegree
            switch self {
            case .BS: degree = .BS
            case .MS: degree = .MS
            case .MENG: degree = .MENG
            case .NA: degree = .NA
            case .Other: degree = .Other
            }
            return degree
        }
    }
}
