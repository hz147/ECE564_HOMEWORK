import UIKit

class TableViewController: UITableViewController {
    var teams = [Team]()
    var selectedCellIndexPath: IndexPath! = IndexPath()
    var sortOrder: SortOrder = .byASC
    var createAnAlertForPersonWithSameFullNameExists: Bool = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadORInitData()
        sortTeamsMembers(byOrder: sortOrder)
        setupNavAndSeperator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if createAnAlertForPersonWithSameFullNameExists {
            createAlert(title: "Opps! A person with the same name already exists", message: "If you would like to modify his/her information, go to the tableView to select his/her cell and edit there")
            createAnAlertForPersonWithSameFullNameExists = false
        }
    }
}

// MARK: - Table view data source

extension TableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return teams.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        
        let person = teams[section].members[row]
        let personInfo = PersonInfoCellEntry(duker: person)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "personInfoCell", for: indexPath) as! PersonInfoCell
        cell.setPersonInfoCell(personInfo: personInfo)
        
        return cell
    }
}

//MARK: - tableView headers(seperator cells)

extension TableViewController {
    
    var teamNames: [String] {
        var teamNames = [String]()
        for team in teams {
            teamNames.append(team.name)
        }
        return teamNames
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return teamNames[section]
    }
    
//    MARK: - header styling
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(red:0.30, green:0.43, blue:0.83, alpha:1.0)
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: "Copperplate", size: 22)
    }
}

//MARK: - action: click a cell

extension TableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        
        let person = teams[section].members[row]
        selectedCellIndexPath = indexPath
        performSegue(withIdentifier: "cellToDetail", sender: person)
    }
}

//  MARK: - action: swipe left to delete

extension TableViewController {
    
    @available(iOS 11.0, *)
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    @available(iOS 11.0, *)
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            let section = indexPath.section
            let row = indexPath.row
            
            let fromWhichTeam = self.teams[section]
            let memberRemains = fromWhichTeam.count
            
            if section > 2 {
                if memberRemains <= 1 {
                    self.teams[section].members.remove(at: row)
                    self.teams.cleanEmptyTeams()
                    let indexSet = IndexSet([section])
                    self.tableView.deleteSections(indexSet, with: .fade)
                } else {
                    self.teams[section].members.remove(at: row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            } else {
                self.teams[section].members.remove(at: row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }

            self.teams.saveAsJson()
            completion(true)
        }
        action.backgroundColor = UIColor.red
        action.image = UIImage(named: "rubbish-bin")
        
        return action
    }
}

// MARK: - tableView styling

extension TableViewController {
    
    func setupNavAndSeperator() {
        
        //setup Nav
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let navigationTitleFont = UIFont(name: "Copperplate", size: 30)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: navigationTitleFont!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        setupSeperatorLine()
    }
    
    func setupSeperatorLine() {
        self.tableView.separatorColor = UIColor.darkGray
        self.tableView.separatorStyle = .singleLine
    }
    
    
}

//MARK: - perform segue(jump to other ViewController)

extension TableViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cellToDetail" {
            if let navController = segue.destination as? UINavigationController {
                if let destVC = navController.topViewController as? DetailInfoViewController {
                    destVC.person = sender as? DukePerson
                    destVC.doPersonInfoDisplayAtInit = true
                    destVC.makeAllTextFieldsIneditable = true
                }
            }
        } else if segue.identifier == "addButtonToDetailInfo" {
            if let navController = segue.destination as? UINavigationController {
                if let destVC = navController.topViewController as? DetailInfoViewController {
                    destVC.doPersonInfoDisplayAtInit = false
                    destVC.rightBarButtonCurrentStatus = .add
                }
            }
        }
    }
}

//MARK: - segue Anchor(return from other ViewController)

extension TableViewController {
    
    @IBAction func returnFromPersonDetailInfoAndDoNothing(segue: UIStoryboardSegue) {
    }
    
    @IBAction func returnFromPersonDetailInfoAndSaveEditedDuker(segue: UIStoryboardSegue) {
        sortTeamsMembers(byOrder: sortOrder)
        teams.saveAsJson()
    }
}

// MARK: - Data initializer

extension TableViewController {
    
    func loadORInitData() {
        if [Team].checkIfSavedJsonExists() {
            if let jsonTeams = [Team].loadFromJson() {
                teams = jsonTeams
            }
        } else {
            firstTimeDataInit()
        }
        teams.cleanEmptyTeams()
        teams.saveAsJson()
    }
    
    func firstTimeDataInit() {
        // init 3 empty arrays for .Professor .TA .Student
        let professors = Team(name: DukeRole.Professor.rawValue)
        let TAs = Team(name: DukeRole.TA.rawValue)
        let students = Team(name: DukeRole.Student.rawValue)
        teams.append(contentsOf: [professors, TAs, students])
        
        let me = DukePerson(firstName: "Haohong", lastName: "Zhao", whereFrom: "China", gender: .Male, role: .Student, hobbies: ["biking", "swimming", "aikido"], academicDegree: .MS, proficientProgrammingLanguage: ["C", "C++", "Java"], team: "MoveIt")
        let professorRic = DukePerson(firstName: "Ric", lastName: "Telford", whereFrom: "Morrisville, NC", gender: .Male, role: .Professor, hobbies: ["golf", "swimming", "biking"], academicDegree: .BS, proficientProgrammingLanguage: ["Swift", "C", "C++"], team: "")
        let TAWalker = DukePerson(firstName: "Walker", lastName: "Eacho", whereFrom: "Chevy Chase, Maryland", gender: .Male, role: .TA, hobbies: ["sailing", "climbing", "baking"], academicDegree: .BS, proficientProgrammingLanguage: ["Swift", "Objective-C", "JavaScript", "C"], team: "")
        let TANiral = DukePerson(firstName: "Niral", lastName: "Shah", whereFrom: "Central New Jersey", gender: .Male, role: .TA, hobbies: ["Computer Vision projects", "Tennis", "Traveling"], academicDegree: .MS, proficientProgrammingLanguage: ["Swift", "Python", "Java", "C"], team: "")
        let AllenBaker = DukePerson(firstName: "Allen", lastName: "Baker", whereFrom: "Central Seattle", gender: .Female, role: .Student, hobbies: ["Traveling"], academicDegree: .Other, proficientProgrammingLanguage: [], team: "")
        
        teams.addToClass(person: me)
        teams.addToClass(person: professorRic)
        teams.addToClass(person: TAWalker)
        teams.addToClass(person: TANiral)
        teams.addToClass(person: AllenBaker)
    
    }
}

//MARK: Add/Sort teams, search on person's first-name

extension Array where Element == Team {
    
    //    MARK: - Add person
    
    mutating func addToClass(person: DukePerson) {
        if !alreadyExists(person: person) {
            for (i, team) in self.enumerated() {
                if team.eligibleToAdd(person: person) {
                    self[i].addMember(member: person)
                    return
                }
            }
            let newTeam = Team(members: [person])
            self.append(newTeam)
        }
    }
    
    func alreadyExists(person: DukePerson) -> Bool {
        for team in self {
            if team.doesExist(person: person) {
                return true
            }
        }
        return false
    }
    
//    MARK: - sort
    
    func sort(sortOrder: SortOrder) {
        for team in self {
            team.sort(sortOrder: sortOrder)
        }
    }
    
    // MARK: - clean empty teams
    mutating func cleanEmptyTeams() {
        for (i, team) in self.enumerated() {
            if (i > 2 && team.members.count == 0) {
                self.remove(at: i)
            }
        }
    }
    
//    MARK: - search by first-name
    
    func searchByFirstName(name: String) -> [Team] {
        var teams = [Team]()
        
        for team in self {
            let filteredteamMembers = team.members.filter({ (person) -> Bool in
                person.firstName.lowercased().contains(name.lowercased())
            })
            if filteredteamMembers.count > 0 {
                let filteredTeam = Team(members: filteredteamMembers)
                teams.append(filteredTeam)
            }
        }
        
        return teams
    }
    
    // MARK: - return section of a team
    
    func returnSection(name: String) -> Int? {
        for (i, team) in self.enumerated() {
            if team.name == name {
                return i
            }
        }
        return nil
    }
}

extension TableViewController {

    func sortTeamsMembers(byOrder: SortOrder) {
        teams.sort(sortOrder: byOrder)
        self.tableView.reloadData()
    }
}

// MARK: - create Alert

extension TableViewController {
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
