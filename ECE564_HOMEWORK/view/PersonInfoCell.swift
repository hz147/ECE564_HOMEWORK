import UIKit

class PersonInfoCell: UITableViewCell {
    @IBOutlet weak var personHeadshotImageView: UIImageView!
    @IBOutlet weak var personFullNameLabelView: UILabel!
    @IBOutlet weak var personDescriptionLabelView: UILabel!
    
    func setPersonInfoCell(personInfo: PersonInfoCellEntry) {
        personHeadshotImageView.image = personInfo.image
        personFullNameLabelView.text = personInfo.fullName
        personDescriptionLabelView.text = personInfo.description
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
