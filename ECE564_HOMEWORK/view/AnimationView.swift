import UIKit

class AnimationView: UIView {
    let fullMoonLayer = CAShapeLayer()
    let logoLayer = CAShapeLayer()
    
    let lineWidth: CGFloat = 3.0
    
    override func draw(_ rect: CGRect) {
        let backgroundSky = UIImage(named: "background-sky")!
        let entireScreen = UIScreen.main.bounds
        backgroundSky.draw(in: entireScreen)
        
        setup()
    }
    
    
    func setup() {
        layer.addSublayer(fullMoonLayer)
        layer.addSublayer(logoLayer)
        let moonSparkLayer = CAShapeLayer()
        fullMoonLayer.addSublayer(moonSparkLayer)
        
        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 100, height: 100).insetBy(dx: lineWidth / 2, dy: lineWidth / 2))
        fullMoonLayer.path = path.cgPath
        fullMoonLayer.fillColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1.0).cgColor
        fullMoonLayer.strokeColor = UIColor.purple.cgColor
        fullMoonLayer.lineWidth = lineWidth
        fullMoonLayer.bounds = path.bounds
        fullMoonLayer.position.x = 100
        fullMoonLayer.position.y = 120
        
        let pathForMoonSpark = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 20, height: 20))
        moonSparkLayer.path = pathForMoonSpark.cgPath
        moonSparkLayer.fillColor = UIColor.white.cgColor
        moonSparkLayer.strokeColor = UIColor.clear.cgColor
        moonSparkLayer.bounds = pathForMoonSpark.bounds
        moonSparkLayer.position.x = 35
        moonSparkLayer.position.y = 35
        
        let pathForLogo = UIBezierPath()
        pathForLogo.move(to: CGPoint(x: 0, y: 0))
        pathForLogo.addLine(to: CGPoint(x: 20, y: 80))
        pathForLogo.addLine(to: CGPoint(x: 60, y: 80))
        pathForLogo.addQuadCurve(to: CGPoint(x: 80, y: 0), controlPoint: CGPoint(x: 80, y: 80))
        pathForLogo.addLine(to: CGPoint(x: 50, y: 55))
        pathForLogo.addLine(to: CGPoint(x: 40, y: 0))
        pathForLogo.addLine(to: CGPoint(x: 30, y: 55))
        pathForLogo.addLine(to: CGPoint(x: 0, y: 0))
        pathForLogo.close()
        
        logoLayer.path = pathForLogo.cgPath
        logoLayer.fillColor = UIColor(red: 0.7059, green: 0.7569, blue: 0.5765, alpha: 1.0).cgColor
        logoLayer.strokeColor = UIColor.yellow.cgColor
        logoLayer.lineJoin = kCALineJoinRound
        logoLayer.lineWidth = 3
        logoLayer.bounds = pathForLogo.bounds
        logoLayer.position.x = bounds.maxX - 50
        logoLayer.position.y = bounds.maxY - 60
        
    }
 

}
