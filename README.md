### HW1  
I have made 5 DukePerson Entries : me, professor Ric, TA Walker, TA Niral and a guy called Allen Baker.
My program contains the following:
* array, class, function, closure expression, enum
* string, character, int, double, bool
* for/in, while, if/else, switch/case
* assignment, arithmatic, comparison, range

I have also included some extra features:
* "He" or "She" depends on the gender property of DukePerson. See Allen Baker's output as an example.
* When there is no proficient langauge, or there is only one . The output will change accordingly. See Allen Baker's output as an example. Same rule applys to hobbies.
* When there is no such a person, the result is changed accordingly
* I have added several statistic properties into DukePerson, including starting letter of his/her firstname, the number of his/her hobbies and proficient languages, lucky number(sum up his/her firstname's letters' ASCII and divide by the number of letters in his/her firstname, provided in Double)
* To show statistics, uncomment lines at bottom of playground
* statistics output is in fine English grammar(He/She, his/her, single/plural form)

### HW2
I have done the following:
* clear compiler warning messages
* add basic error check in input to avoid app crash
* prevent the keyboard from blocking next input

Additionally, my app has several highlights:
* add a pickerView to the "gender" textfield
* add intermediate error check to avoid app crash
* put effort into layout design as well as style, font & color
* add an imageView of Duke university
* make the "add" button to "update/add" button
* add response to user when adding, updating or finding to improve user experience
* refactor my code into functions

### HW3
I have created a simple Peson Information Book-keeping app. 

My app has a few highlights:
* In the tableView, cells are sorted and then displayed. The sorting order is ASC by default. The sorting order is in the tableView class at present.
* Well-designed UI. I carefully selected colors and fonts to provide a good user experience.
* Error messages are now in pop-up windows. I handled two errors this way: 
    1. Invlid input(s) in the edit/add page. 
    2. Add a person that is already exist. (If two people's full name are the same, I consider these two people the same guy)

### HW4
I added an animationView which displayed an animation about one of my hobbies: "Aikido"(a martial art from Japan). I drew a crown-like logo and a full-moon using vector graph. The animation character was Ryu from "the Street Fighter" Series. The background music I added was also the theme for Ryu. I collected the background image, the background music and the animation chracter gifs from the Internet.

TODO:
1. add a customized viewController transition for the transition from my detail-info view to the animation view.

This time, the highlights of my app are:
1. a background music which makes the animation view vivid.
2. the animation for Ryu has two parts: first he did a punch, and then a kick. I made the action by animation chain.
3. The moon and the logo are drawn using CAlayers. I have started learning Core Graphics in swift.
4. The button: "jump to animation view" on the buttom-right corner of my detail-info view is styled using CAlayer techniques which I learned from a video course series this weekend(Sep 29th, 30th).

### HW5
Notice: To test with some data, you may need to re-install the app

I have done some basic tests: 
    1. add new person to a existing or none-existing team
    2. change a person's team
    3. delete the last remaining person in Professor, TA, Student
    4. delete the last remaining person in a team

The search bar is not working now.
I have tried to add a search bar but failed. The problem I met was that: I was confused in keeping sync between two data sources(one for searching). I didn't know how to keep sync when deleting from the search view. 


This time, the highlights of my app are:
1. better data model: add a Team class, and now the data are stored as [Team]
2. code refactoring: now the code is much more compact
3. In show/edit view, change some input-fields from text-fields to segmented controllers

