import UIKit

/*:
 ### ECE 564 HW1 assignment
 In this first assignment, you are going to create a base data model for storing information about the students, TAs and professors in ECE 564. You need to populate your data model with at least 3 records, but can add more.  You will also provide a search function ("whoIs") to search an array of those objects by first name and last name.
 I suggest you create a new class called `DukePerson` and have it subclass `Person`.  You also need to abide by 2 protocols:
 1. BlueDevil
 2. CustomStringConvertible
 
 I also suggest you try to follow good OO practices by containing any properties and methods that deal with `DukePerson` within the class definition.
 */
/*:
 In addition to the properties required by `Person`, `CustomStringConvertible` and `BlueDevil`, you need to include the following information about each person:
 * Their degree, if applicable
 * Up to 3 of their best programming languages as an array of `String`s (like `hobbies` that is in `BlueDevil`)
 */
/*:
 I suggest you create an array of `DukePerson` objects, and you **must** have at least 4 entries in your array for me to test:
 1. Yourself
 2. Me (my info is in the Class Handbook)
 3. The TAs (also in Class Handbook)
 */
/*:
 Your program must contain the following:
 - You must include 4 of the following - array, dictionary, set, class, function, closure expression, enum, struct
 - You must include 4 different types, such as string, character, int, double, bool, float
 - You must include 4 different control flows, such as for/in, while, repeat/while, if/else, switch/case
 - You must include 4 different operators from any of the various categories of assignment, arithmatic, comparison, nil coalescing, range, logical
 */
/*:
 Base grade is a 45 but more points can be earned by adding additional functions besides whoIs (like additional search), extensive error checking, concise code, excellent OO practices, and/or excellent, unique algorithms
 */
/*:
 Below is an example of what the string output from `whoIs' should look like:
 
 Ric Telford is from Morrisville, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Biking, Hiking and Golf.
 */

enum Gender {
    case Male
    case Female
}

class Person {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
}

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}
//: ### START OF HOMEWORK
//: Do not change anything above.
//: Put your code here:

enum AcademicDegree : String {
    case BSc = "BSc."
    case MSc = "MSc."
    case Phd = "Phd."
}

class DukePerson: Person, BlueDevil, CustomStringConvertible {
    var role : DukeRole = .Student
    var hobbies : [String] = ["nothing"]
    var academicDegree : AcademicDegree = .BSc
    var proficientProgrammingLanguage: [String] = ["no programming language"]
    var description: String {
        let fullName = "\(self.firstName) \(self.lastName)"
        var he = "He"
        if gender == .Female {
            he = "She"
        }
        let hobbiesString = self.stringListArray2String(stringListArray: self.hobbies)
        let proficientLanguagesString = self.stringListArray2String(stringListArray:self.proficientProgrammingLanguage)
        
        return "\(fullName) is from \(whereFrom) and is a \(role). \(he) is proficient in \(proficientLanguagesString). When not in class, \(firstName) enjoys \(hobbiesString)."
    }
    
    var numberOfHobbies : Int = 0
    var numberOfProficientLanguages : Int = 0
    var isUniversityStaff : Bool = false
    var firstLetterOfFirstName : Character = "A"
    var luckyNumberInDouble : Double = 0
    
    init(firstName : String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, hobbies: [String], academicDegree: AcademicDegree, proficientProgrammingLanguage: [String]) {
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
        self.role = role
        if hobbies.count != 0 {
            self.hobbies = hobbies
        }
        self.academicDegree = academicDegree
        if proficientProgrammingLanguage.count > 3 {
            self.proficientProgrammingLanguage = ["NA", "NA", "NA"]
            for i in 0...2 {
                self.proficientProgrammingLanguage[i] = proficientProgrammingLanguage[i]
            }
        } else if proficientProgrammingLanguage.count != 0 {
            self.proficientProgrammingLanguage = proficientProgrammingLanguage
        }
        
        if hobbies.count != 0 {
            self.numberOfHobbies = self.hobbies.count
        }
        if proficientProgrammingLanguage.count != 0 {
            self.numberOfProficientLanguages = self.proficientProgrammingLanguage.count
        }
        if self.role == .Professor {
            self.isUniversityStaff = true
        }
        self.firstLetterOfFirstName = firstName[firstName.startIndex]
        self.luckyNumberInDouble = self.calculateLuckyNumber()
    }
    
    func stringListArray2String(stringListArray: [String]) -> String {
        var ans = ""
        switch stringListArray.count {
        case 0:
            ans = ""
        case 1:
            ans = stringListArray[0]
        default:
            let arrayLength = stringListArray.count
            for i in 0..<stringListArray.count-2 {
                ans += "\(stringListArray[i]), "
            }
            ans += "\(stringListArray[arrayLength-2]) and \(stringListArray[arrayLength-1])"
        }
        return ans
    }
    
    func whoIs() -> String {
        return description
    }
    
    func calculateLuckyNumber() -> Double {
        var i = 0
        var sum : Double = 0
        while i < firstName.count {
            var str = String(firstName[firstName.index(firstName.startIndex, offsetBy: i)])
            var number:Int = 0
            for code in str.unicodeScalars {
                number = Int(code.value);
            }
            sum += Double(number)
            i+=1
        }
        return sum/Double(firstName.count)
    }
    
    func statistic() -> String {
        let fullName = "\(self.firstName) \(self.lastName)"
        var isA = "is a"
        if self.isUniversityStaff == false {
            isA = "is not a"
        }
        var he = "He"
        var his = "His"
        if self.gender == .Female {
            he = "She"
            his = "Her"
        }
        var languages = "language"
        if numberOfHobbies >= 2 {
            languages += "s"
        }
        var hobbies = "hobby"
        if numberOfHobbies >= 2 {
            hobbies = "hobbies"
        }
        return "\(fullName) \(isA) a Duke University Staff. \(his) first name starts with letter \(firstLetterOfFirstName). \(he) has \(numberOfHobbies) \(hobbies) and \(numberOfProficientLanguages) proficient \(languages). \(his) lucky number is \(luckyNumberInDouble)."
    }
    
}

var me = DukePerson(firstName: "Haohong", lastName: "Zhao", whereFrom: "China", gender: .Male, role: .Student, hobbies: ["biking", "swimming", "running"], academicDegree: .MSc, proficientProgrammingLanguage: ["C", "C++", "Java"])
var professorRic = DukePerson(firstName: "Ric", lastName: "Telford", whereFrom: "Morrisville, NC", gender: .Male, role: .Professor, hobbies: ["golf", "swimming", "biking"], academicDegree: .BSc, proficientProgrammingLanguage: ["Swift", "C", "C++"])
var TAWalker = DukePerson(firstName: "Walker", lastName: "Eacho", whereFrom: "Chevy Chase, Maryland", gender: .Male, role: .TA, hobbies: ["sailing", "climbing", "baking"], academicDegree: .BSc, proficientProgrammingLanguage: ["Swift", "Objective-C", "JavaScript", "C"])
var TANiral = DukePerson(firstName: "Niral", lastName: "Shah", whereFrom: "Central New Jersey", gender: .Male, role: .TA, hobbies: ["Computer Vision projects", "Tennis", "Traveling"], academicDegree: .MSc, proficientProgrammingLanguage: ["Swift", "Python", "Java", "C"])
var AllenBaker = DukePerson(firstName: "Allen", lastName: "Baker", whereFrom: "Central Seattle", gender: .Female, role: .Student, hobbies: ["Traveling"], academicDegree: .Phd, proficientProgrammingLanguage: [])

var Dukers : [DukePerson] = [me, professorRic, TAWalker, TANiral, AllenBaker]

func whoIs(_ fullNameToSearch : String) -> String {
    for i in 0..<Dukers.count {
        let fullName = "\(Dukers[i].firstName) \(Dukers[i].lastName)"
        if fullName == fullNameToSearch {
            return Dukers[i].whoIs()
        }
    }
    return "I haven't found a person called \(fullNameToSearch)"
}

print("\nFinding Haohong Zhao\n")
print(whoIs("Haohong Zhao"))
print("\nFinding Ric Telford\n")
print(whoIs("Ric Telford"))
print("\nFinding Walker Eacho\n")
print(whoIs("Walker Eacho"))
print("\nFinding Niral Shah\n")
print(whoIs("Niral Shah"))
print("\nFinding Allen Baker\n")
print(whoIs("Allen Baker"))
print("\nFinding Secret Person\n")
print(whoIs("Secret Person"))

//: ### UNCOMMENT TO SEE STATISTICS
//print("\nPrinting Haohong Zhao's statistic\n")
//print(me.statistic())
//print("\nPrinting Ric Telford's statistic\n")
//print(professorRic.statistic())
//print("\nPrinting Walker Eacho's statistic\n")
//print(TAWalker.statistic())
//print("\nPrinting Niral Shah's statistic\n")
//print(TANiral.statistic())
//print("\nPrinting Allen Baker's statistic\n")
//print(AllenBaker.statistic())


//: ### END OF HOMEWORK
//: Uncomment the line below to test your homework.
//: The "whoIs" function should be defined as `func whoIs(_ name: String) -> String {   }`

// print(whoIs("Ric Telford"))
